#ifndef PLUGINSPHEROIDMANAGER_HPP
#define PLUGINSPHEROIDMANAGER_HPP

/**
 * @file PluginSpheroidManager.hpp
 * @brief Defines the PluginSpheroidManager class for managing spheroid bodies.
 *
 * Contains the spheroid manager plugin class.
 */

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/point_generators_3.h>
#include <CGAL/algorithm.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/convex_hull_3.h>
#include <CGAL/intersections.h>
#include <vector>
#include <mecacell/mecacell.h>
#include <math.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Polyhedron_3<K> Polyhedron_3;
// define point creator
typedef K::Point_3 Point_3;
typedef K::Line_3 Line_3;

/**
 * @struct PlaneFromFacet
 * @brief Functor computing the plane containing a triangular facet.
 */
struct PlaneFromFacet
{
    Polyhedron_3::Plane_3 operator()(Polyhedron_3::Facet &f)
    {
        Polyhedron_3::Halfedge_handle h = f.halfedge();
        return Polyhedron_3::Plane_3(h->vertex()->point(),
                                     h->next()->vertex()->point(),
                                     h->opposite()->vertex()->point());
    }
};

/**
 * @namespace SpheroidManager
 * @brief Namespace for spheroid manager-related classes and functions.
 */
namespace SpheroidManager {

    /**
     * @class PluginSpheroidManager
     * @brief Class for managing spheroid bodies.
     * 
     * @tparam cell_t Type of the cell.
     */
    template<typename cell_t>
    class PluginSpheroidManager {
        typedef std::vector<double> Data;

    private:
        MecaCell::Vec centroid; /**< Center of gravity */
        double optimRad = 0.0; /**< Radius of the spheroid */
        double meanRadius; /**< Mean radius of the cells in µm */

        /**
         * @brief Gaussian probability density function.
         *
         * µ = 0 and sigma = 1
         *
         * @param x Input value.
         * @return Probability density at x.
         */
        inline static double Kernel(const double x) { return exp(- pow(x, 2) / 2.) / sqrt(2. * M_PI); }

        /**
         * @brief Updates the distance from centroid for each cell.
         *
         * @param cells Vector of cell pointers.
         */
        void  updateDistanceFromCentroid(std::vector<cell_t*> cells) {
            double r = 0;
            Data data;
            std::vector<Point_3> points;
            double max = - INFINITY;
            for (auto &c : cells) {
                r = (c->getBody().getPosition() - centroid).length();
                c->getBody().setDistanceFromCentroid(r);
                data.push_back(r);
                points.push_back(Point_3(c->getBody().getPosition().x(), c->getBody().getPosition().y(), c->getBody().getPosition().z()));
                if (r > max) {
                    max = r;
                }
            }
            optimRad = getOptimalRadius(data, max);

            // define polyhedron to hold convex hull
            Polyhedron_3 poly;

            // compute convex hull of non-collinear points
            CGAL::convex_hull_3(points.begin(), points.end(), poly);

            if (poly.size_of_vertices() > 5) {
                std::transform(poly.facets_begin(), poly.facets_end(), poly.planes_begin(), PlaneFromFacet());
                Point_3 pCentroid = Point_3(centroid.x(), centroid.y(), centroid.z());
                for (auto &c : cells) {
                    Point_3 pCell = Point_3(c->getBody().getPosition().x(), c->getBody().getPosition().y(),
                                            c->getBody().getPosition().z());
                    Line_3 lCellCentroid = Line_3(pCell, pCentroid);
                    Polyhedron_3::Plane_iterator pit;
                    double min = INFINITY;
                    for (pit = poly.planes_begin(); pit != poly.planes_end(); ++pit) {
                        auto result = CGAL::intersection((*pit), lCellCentroid);
                        if (result) {
                            const Point_3 *p = boost::get<Point_3>(&*result);
                            double dist = CGAL::squared_distance(pCell,(*p));
                            if (dist < min) {
                                min = dist;
                            }
                        }
                    }
                    c->getBody().setDistanceFromCentroid( c->getBody().getDistanceFromCentroid() * optimRad /
                                                        (c->getBody().getDistanceFromCentroid() + sqrt(min) ));
                }
            }
        }

        /**
         * @brief Computes the optimal radius.
         *
         * @param data Vector of distances from centroids of each cell.
         * @param max Maximal distance from centroid.
         * @return Optimal radius.
         */
        double getOptimalRadius(const Data &data, double max) {
            double maxDensity = -1.;
            double xMaxDensity = -1.;
            double val;
            double lambda = 10.;
            double sum;
            for (double i = 0.; i <= max; i += 0.2) {
                sum = 0;
                for (size_t j = 0; j < data.size(); ++j) {
                    sum += Kernel((i - data[j]) / lambda);
                }
                val = sum / lambda;
                if (val > maxDensity) {
                    maxDensity = val;
                    xMaxDensity = i;
                }
            }
            return (max - (max - xMaxDensity) / 2 + meanRadius);
        }

       /**
        * @brief Computes the centroid position.
        *
        * @param cells Vector of cell pointers.
        * @return Centroid position.
        */
        MecaCell::Vec computeCentroid(std::vector<cell_t*> cells) {
            centroid = MecaCell::Vec(0, 0, 0);
            for (auto &c : cells) {
                centroid += c->getBody().getPosition();
            }
            return centroid /= cells.size();
        }

    public:
        /**
         * @brief Default constructor.
         */
        inline PluginSpheroidManager() = default;

        /**
         * @brief Constructor with mean radius.
         * @param r Mean radius in µm.
         */
        inline PluginSpheroidManager(double r) : meanRadius(r){}

        /**
         * @brief Gets the centroid.
         * @return Centroid position.
         */
        inline MecaCell::Vec getCentroid() const{ return centroid; }

        /**
         * @brief Gets the spheroid radius.
         * @return Spheroid radius.
         */
        inline double getSpheroidRadius() const{ return (optimRad); }

        /**
         * @brief Sets the mean radius.
         * @param r Mean radius.
         */
        inline void setMeanRadius(double r) { meanRadius = r; }


        /**
         * @brief Computes everything for the spheroid.
         *
         * Updates centroid position and spheroid radius.
         *
         * @param cells Vector of cell pointers.
         */
        void updateSpheroid(std::vector<cell_t*> cells) {
            if (cells.size() > 5) {
                computeCentroid(cells);
                updateDistanceFromCentroid(cells);
            }
        }

        /**
         * @brief Init function to be called on scenario init.
         *
         * Computes the spheroid information on the cells to be added.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void initSpheroid(world_t *w) {
            updateSpheroid(w->newCells);
        }

        /**
         * @brief Pre-behavior update hook for MecaCell.
         *
         * Computes the spheroid information.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void preBehaviorUpdate(world_t *w) {
            updateSpheroid(w->cells);
        }
    };
}
#endif
