#ifndef BODYSPHEROIDMANAGER_HPP
#define BODYSPHEROIDMANAGER_HPP

#include "../../../../src/core/BaseBody.hpp"

/**
 * @file BodySpheroidManager.hpp
 * @brief Defines the BodySpheroidManager class for managing spheroid bodies.
 *
 * Contains the body class which works with the SpheroidManager plugin.
 */

/**
 * @namespace SpheroidManager
 * @brief Namespace for spheroid manager-related classes and functions.
 */
namespace SpheroidManager {

    /**
     * @class BodySpheroidManager
     * @brief Class for managing spheroid bodies.
     * 
     * @tparam cell_t Type of the cell.
     * @tparam plugin_t Type of the plugin.
     */
    template<typename cell_t, class plugin_t>
    class BodySpheroidManager : public virtual BaseBody<plugin_t> {

    private:
        double distanceFromCentroid; /**< Distance from centroid in µm */

    public:

        /**
         * @brief Default constructor.
         */
        inline BodySpheroidManager() = default;

        /**
         * @brief Gets the distance from the centroid.
         * 
         * @return Distance from the centroid.
         */
        inline double getDistanceFromCentroid() { return distanceFromCentroid; }

        /**
         * @brief Sets the distance from the centroid.
         * 
         * @param d Distance from the centroid.
         */
        inline void setDistanceFromCentroid(double d) { distanceFromCentroid = d; }

    };

}

#endif // BODYSPHEROIDMANAGER_HPP
