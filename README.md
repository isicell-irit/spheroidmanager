# Spheroid Manager

This folder contains the body and plugin classes that could be used to manage a spheroid simulation on cells using *MecaCell*.

## Prerequisites

- CGAL 4.13
- work on 3D *MecaCell* simulation
- have a general body class that will inherit the **BodySpheroidManager** class, this class also needs to inherit the *MecaCell* **Movable** class
- have a general plugin class that will use the **PluginSpheroidManager** class

## How to use it

- include the folder in your *MecaCell* project  
- import the **BodySpheroidManager.hpp** file in the file containing your simulation general body class  
- make your general body class inherit from the **BodySpheroidManager** class
```cpp 
template <class cell_t> class CellBody : public BodySpheroidManager
 ```  
- in your general body class constructor add the following line :
 ```cpp 
 explicit CellBody(cell_t *, MecaCell::Vec pos = MecaCell::Vec::zero())
 : BodySpheroidManager()
 ```
- import the **PluginSpheroidManager.hpp** file in the file containing your general plugin class or struct  
- add a **PluginSpheroidManager** attribute to your  general plugin class or struct  
```cpp  
PluginSpheroidManager spheroidManager = pluginSpheroidManager(radius);  
```  
radius corresponds to the mean radius of the simulated cells in **µm**
- in the onRegister method, add the new attribute  
```cpp  
template <typename world_t> void onRegister(world_t* w){    
w->registerPlugin(spheroidManager); }  
```
Your simulation will now implement cells that have access to their distance from the center of the spheroid.
You can also get the radius of your spheroid by calling the ro getter from your plugin.
For example, within a scenario class :
```cpp
w.cellPlugin.spheroidManager.getSpheroidRadius()
```